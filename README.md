# weekly competitions
The data is related with direct marketing campaigns of a Portuguese banking institution. The marketing campaigns were based on phone calls. Often, more than one contact to the same client was required, in order to access if the product (bank term deposit) would be ('yes') or not ('no') subscribed. 

Dataset.
bank-full.csv with all examples and 17 inputs, ordered by date (older version of this dataset with less inputs).


Your task is to explore the data and do some visualizations.
Dataset can be downloaded from the below link
https://raw.githubusercontent.com/ummarshaik/datasets/master/bank-full.csv'
Kindly copy this url and paste in browser to access this data.

